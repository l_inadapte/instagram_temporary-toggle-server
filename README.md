# Instagram temporary-toggle-server

> THIS SCRIPT IS MADE FOR WORKING WITH MULLVAD-VPN ONLY.  
> But it can be easily modified to be use with another VPN.

Note:  
The easy way to use this script is to associate it with a keyboard shortcut as < Super + I > (e.g.).

---

This script allows to temporarily toggle from one Mullvad server to another with a countdown.  
It's useful when Instagram forbids your actual VPN server.  

To connect to Instagram, you just have to run this script who gives you 'x' secondes/minutes to enter your credentials.  
It will go through Houston (e.g.) and then switching back to Paris (e.g.) after at the end of the countdown.  

---
anorax, 2023   
*noob forever*   
[on Mastodon](https://framapiaf.org/@l_inadapte "my profile on Mastodon") 